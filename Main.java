import java.util.*;

public class Main {
    public static void main(String[] args) {
        ArrayList<Question> listQuestions = new ArrayList<Question>();
        listQuestions.add(new Question("Sur quelle échelle est exprimée l’intensité des tremblements de terre ?",
                "L’échelle de Richter", 3));
        listQuestions.add(new Question("Combien d’os constituent le squelette d’un homme ou d’une femme adulte ?",
                "206", 3));
        listQuestions.add(new Question("Laïka était le premier animal quia fait le tour de la Terre dans un " +
                "engin spatial. Quel animal était Laïka ?", "Un chien", 3));
        listQuestions.add(new Question("Comment s’appelle la statuette qui est remise chaque année à Hollywood " +
                "pour le meilleur film ?", "L’Oscar", 1));
        listQuestions.add(new Question("Comment s’appelle l’organisation qui aide les enfants dans le monde entier ?",
                "UNICEF", 2));
        listQuestions.add(new Question("Avec quelle lettre les Romains représentaient-ils le chiffre 500 ?",
                "D", 1));
        listQuestions.add(new Question("En Afrique, on trouve des chutes d’eau qui portent un prénom féminin. Lequel ?",
                "Victoria", 2));
        listQuestions.add(new Question("Parquel village anglais passe le méridien 0 ?", "Greenwich", 2));
        listQuestions.add(new Question("Combien de quilles y a-t-il sur la piste de bowling ?", "10", 1));
        listQuestions.add(new Question("À combien d’œufs de poul eéquivaut un œuf d’autruche ?", "25", 3));

        Play players = new Play();
        int roundsPlayers = players.listPlayers.size();
        String userAnswer = "";
        //rounds for each player
        for(int x = 0; x < roundsPlayers; x++)
            //get question, try it, update score
            for (Question q : listQuestions) {
            System.out.println(q.getQues());
            Scanner anUser = new Scanner(System.in);
            userAnswer = anUser.nextLine();
            Boolean result = q.tryAnswer(userAnswer);
            //System.out.println(result);
            if(q.tryAnswer(userAnswer) == true){
                System.out.println("Bravo, good answer!");
                players.listPlayers.add((x+1), ); =+ q.getValue();
            }
            else{
                System.out.println("Shame on you! " + "Try harder next time");
                }
            }
}
}


