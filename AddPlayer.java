public class AddPlayer {
    private String namePlayer;
    private int scorePlayer;

    public AddPlayer(String nPlayer, int sPlayer) {
        this.namePlayer = nPlayer;
        this.scorePlayer = sPlayer;
    }
    public String getNamePlayer() {
        return namePlayer;
    }

    public int getScore(){
        return scorePlayer;
    }
}
