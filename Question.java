import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.Scanner;
import java.text.Normalizer;

public class Question {
    protected String ques;
    protected String answer;
    protected int valeur;

    Question(String q, String a, int v) {
        this.ques = q;
        this.answer = a;
        this.valeur = v;
    }

    public String getQues() {
        return ques;
    }

    public int getValue() {return valeur;}

    public Boolean tryAnswer(String userAnswer){
        //StringUtils ansUnaccent = new StringUtils();
        if(answer.toLowerCase().equals(userAnswer.toLowerCase())) {
            return true;
        }
        ;
        return false;
    }

    public class StringUtils {

        public String unaccent(String src) {
            return Normalizer
                    .normalize(src, Normalizer.Form.NFD)
                    .replaceAll("[^\\p{ASCII}]", "");
        }

    }
}